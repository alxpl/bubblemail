# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2012 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
import os
import logging
import hashlib
from uuid import uuid4 as generate_uuid
from datetime import datetime

import gi
gi.require_version('Secret', '1')
gi.require_version('GLib', '2.0')
# pylint: disable=C0413
from gi.repository import Secret, GLib
try:
    gi.require_version('Goa', '1.0')
    from gi.repository import Goa
except (ValueError, ModuleNotFoundError):
    logging.warning('No gnome online accounts support found')
    Goa = None  # pylint: disable=C0103

from .config import PACKAGE_NAME, CACHE_DIR, validate_folders
from .utils import pickle_read, pickle_write
from .i18n import _


class SecretStore:
    def __init__(self):
        self.schema = Secret.Schema.new(
            f'org.framagit.{PACKAGE_NAME}', Secret.SchemaFlags.NONE,
            {"uuid": Secret.SchemaAttributeType.STRING})

    def save(self, account_name, uuid, password):
        if not uuid or not password:
            logging.error('Missing uuid or password')
            return False
        try:
            Secret.password_store_sync(
                self.schema, {'uuid': uuid}, Secret.COLLECTION_DEFAULT,
                f'{PACKAGE_NAME.capitalize()} secret source for {account_name}',
                password, None)
        except GLib.GError:
            logging.error('Keyring seems locked !')
            return False
        else:
            return True

    def password(self, uuid):
        return Secret.password_lookup_sync(self.schema, {'uuid': uuid}, None)

    def clear(self, uuid):
        return Secret.password_clear_sync(self.schema, {'uuid': uuid}, None)


class Account(dict):
    SECTION_NAME = 'Account '
    UUID = 'uuid'
    NAME = 'name'
    TYPE = 'type'
    ENABLED = 'enabled'
    BACKEND = 'backend'
    FOLDERS = 'folders'
    INTERNAL = 'internal'
    WEBMAIL = 'webmail'
    GOA = 'goa'
    IMAP = 'imap'
    POP3 = 'pop3'
    MAILDIR = 'maildir'
    MBOX = 'mbox'
    ERR_LAST = 'error_last'
    ERR_COUNT = 'error_count'
    ERR_CODE = 'error_code'
    ERR_MSG = 'error_message'
    STATUS = 'status'
    FETCH_COUNT = 'Fetch counter'

    KEYS = (NAME, TYPE)
    OPT_KEYS = (ENABLED, FOLDERS, WEBMAIL)
    UNSAVED_KEYS = (FOLDERS, STATUS)
    BOOLEAN_KEYS = (ENABLED,)

    def __init__(self, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config = config
        for accountkey in self.KEYS + self.OPT_KEYS:
            if accountkey not in self or self[accountkey] == 'None':
                self[accountkey] = ''
        if self.UUID not in self or not self[self.UUID]:
            self[self.UUID] = str(generate_uuid())[:8]
        if not self[self.TYPE]:
            self[self.TYPE] = self.INTERNAL
        if self[self.ENABLED]:
            self[self.ENABLED] = bool(int(self[self.ENABLED]))
        else:
            self[self.ENABLED] = True
        for boolean_key in self.BOOLEAN_KEYS:
            if boolean_key not in self or not self[boolean_key]:
                self[boolean_key] = False
                continue
            self[boolean_key] = bool(int(self[boolean_key]))
        # print(self)
        self.status = AccountStatus(self)
        self[self.FOLDERS] = validate_folders(self[self.FOLDERS])
        self.section = self.SECTION_NAME + self[self.UUID]

    @classmethod
    def _account_class(cls, account):
        account_class = RemoteAccount
        if account[cls.TYPE] == cls.GOA:
            if not Goa:
                logging.warning('No gnome online accounts support found')
                return None
            return GnomeOnlineAccount
        if cls.is_local(account):
            account_class = LocalAccount
        return account_class

    @classmethod
    def get(cls, config, uuid):
        section = cls.SECTION_NAME + uuid
        if not section in config:
            logging.debug('Account %s not found in config', uuid)
            return None
        account = dict(config.items(section))
        account_class = cls._account_class(account)
        if account_class:
            return account_class(config, account)
        return None

    @classmethod
    def list(cls, config):
        A = Account
        sections = filter(lambda s: A.SECTION_NAME in s, config.sections())
        accounts = map(lambda s: dict(config.items(s)), sections)
        for account in sorted(accounts, key=lambda a: a[A.NAME].lower()):
            account_class = cls._account_class(account)
            if not account_class:
                continue
            yield account_class(config, account)

    @classmethod
    def list_remote(cls, config):
        return (account for account in cls.list(config)
                if not account.is_local())

    @classmethod
    def count(cls, config):
        return len(list(cls.list(config)))

    @classmethod
    def is_local(cls, account):
        return account[cls.BACKEND] in (cls.MAILDIR, cls.MBOX)

    @property
    def is_locked(self):
        return False

    @property
    def collect_error(self):
        return self[self.STATUS]

    @property
    def is_valid(self):
        if list(filter(lambda k: k not in self, self.KEYS + self.OPT_KEYS)):
            return False
        if list(filter(lambda k: k not in self.BOOLEAN_KEYS and not self[k],
                       self.KEYS)):
            return False
        if list(filter(lambda k: not isinstance(self[k], bool),
                       filter(lambda k: k in self.KEYS, self.BOOLEAN_KEYS))):
            return False
        return True

    def delete(self, safe_call=True, commit=True, keep_credentials=True):  # pylint: disable=unused-argument
        uuid = self[self.UUID]
        section = self.SECTION_NAME + uuid
        if safe_call and not self.config.has_section(section):
            logging.error(f'Unknown Account with id {uuid}')
            return
        self.config.remove_section(section)
        if commit:
            self.config.save()
        if self.config.dbus_service:
            self.config.dbus_service.Refresh()

    def set_folders(self):
        if not self.config.has_section(self.section):
            return
        if self[self.FOLDERS]:
            self.config.set(self.section, self.FOLDERS, self[self.FOLDERS])
        else:
            self.config.set(self.section, self.FOLDERS, '')

    def save(self, commit=True, create=True):
        if self.config.has_section(self.section):
            self.delete(safe_call=False, commit=False)
        elif not create:
            return
        self.config.add_section(self.section)
        for saved_key in filter(lambda key: key not in self.UNSAVED_KEYS,
                                self.keys()):
            self.config.set(self.section, saved_key, self[saved_key])
        if commit:
            self.config.save()


class LocalAccount(Account):
    PATH = 'path'
    BACKENDS = (Account.MAILDIR, Account.MBOX)

    KEYS = Account.KEYS + (Account.BACKEND, PATH)
    OPT_KEYS = Account.OPT_KEYS
    UNSAVED_KEYS = Account.UNSAVED_KEYS

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        self[self.BACKEND] = self[self.BACKEND] or self.MAILDIR

    @property
    def is_valid(self):
        if not super().is_valid:
            return False
        if not os.path.exists(self[self.PATH]):
            logging.error(f'Path {self[self.PATH]} does not exist')
            return False
        if not os.access(self[self.PATH], os.R_OK):
            return False
        if os.path.isdir(self[self.PATH]) and self[self.BACKEND] == self.MBOX:
            return False
        if os.path.isfile(self[self.PATH]):
            if self[self.BACKEND] == self.MAILDIR:
                return False
        return True

    def set_default_name(self):
        if self[self.NAME] or not self[self.PATH]:
            return
        self[self.NAME] = os.path.basename(self[self.PATH])
        self[self.NAME] = self[self.NAME].replace('.', '')
        self[self.NAME] = self[self.NAME].capitalize()

    def save(self, commit=True, create=True):
        super().save(commit=False, create=create)
        if self[self.BACKEND] == self.MAILDIR:
            self.set_folders()
        if commit:
            self.config.save()
        return True

class RemoteAccount(Account):
    OAUTH2 = 'oauth2'
    USER = 'user'
    PASS = 'password'
    SERVER = 'server'
    PORT = 'port'
    IMAP = 'imap'
    POP3 = 'pop3'
    UNSECURE = 'unsecure'
    PROXY = 'proxy'
    PROXY_TYPE = 'pxytype'
    PROXY_HOST = 'pxyurl'
    PROXY_PORT = 'pxyport'
    PROXY_USER = 'pxyusr'
    PROXY_PASS = 'pxypass'
    BACKENDS = (IMAP, POP3)

    KEYS = Account.KEYS + (SERVER, USER, PASS, Account.BACKEND, PROXY, UNSECURE)
    OPT_KEYS = Account.OPT_KEYS + (PORT, PROXY_TYPE, PROXY_HOST, PROXY_PORT,
                                   PROXY_USER, PROXY_PASS)
    BOOLEAN_KEYS = (UNSECURE, PROXY)
    UNSAVED_KEYS = Account.UNSAVED_KEYS + (PASS,)

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        if not self.BACKEND in self.keys() or not self[self.BACKEND]:
            self[self.BACKEND] = self.IMAP
        self.secret_store = SecretStore()
        if self[self.TYPE] == self.INTERNAL and not self[self.PASS]:
            self[self.PASS] = self.secret_store.password(self[self.UUID])

    @property
    def is_locked(self):
        return not self[self.PASS]

    @property
    def is_valid(self):
        if not super().is_valid:
            return False
        if self[self.BACKEND] not in self.BACKENDS:
            return False
        if self[self.PROXY] and not (
            self[self.PROXY_HOST] and self[self.PROXY_PORT]):
            return False
        return True

    def delete(self, safe_call=True, commit=True, keep_credentials=True):
        super().delete(safe_call, commit, keep_credentials)
        if self[self.TYPE] == self.INTERNAL and not keep_credentials:
            logging.debug(f'Deleting credentials for {self[self.NAME]}')
            self.secret_store.clear(self[self.UUID])

    def save(self, commit=True, create=True):
        if self[self.TYPE] == self.INTERNAL and create:
            logging.debug(f'Saving credentials for {self[self.NAME]}')
            if not self.secret_store.save(self[self.NAME], self[self.UUID],
                                           self[self.PASS]):
                return False
        super().save(commit=False, create=create)
        if self[self.BACKEND] == self.IMAP:
            self.set_folders()
        if commit:
            self.config.save()
        return True


class GnomeOnlineAccount(RemoteAccount):
    OAUTH2 = 'oauth2'
    PROVIDER = 'provider'

    KEYS = Account.KEYS
    OPT_KEYS = RemoteAccount.OPT_KEYS + (Account.ENABLED, Account.FOLDERS)
    UNSAVED_KEYS = RemoteAccount.UNSAVED_KEYS + (
        RemoteAccount.USER, RemoteAccount.SERVER, RemoteAccount.PORT, OAUTH2,
        PROVIDER)

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        self[self.USER] = 'Unknown user'
        self[self.SERVER] = 'Unknown server'
        self.goa_account = None
        goa_account = list(filter(
            lambda a: self.get_uuid(a.get_mail().props) == self[self.UUID],
            self.goa_accounts()))
        if goa_account:
            self.goa_account = goa_account[0]
            self._populate()

    def _populate(self):
        goa_mail = self.goa_account.get_mail().props
        account = self.goa_account.get_account()
        self[self.PROVIDER] = account.props.provider_type
        if self[self.PROVIDER] == 'exchange':
            self[self.SERVER] = self.goa_account.get_exchange().props.host
            user = account.get_cached_property('Identity')
            self[self.USER] = str(user).replace("'", "")
        else:
            self[self.SERVER] = goa_mail.imap_host
            self[self.USER] = goa_mail.imap_user_name
        if not self[self.USER] or not self[self.SERVER]:
            self.status.set(self.status.CREDENTIALS_MISSING)
            logging.error(f'GOA account {self[self.NAME]}: missing credentials')
            logging.error(self)

    def get_credentials(self):
        pwd_based = self.goa_account.get_password_based()
        if pwd_based:
            try:
                self[self.PASS] = pwd_based.call_get_password_sync(
                    'imap-password', None)
            except GLib.GError as error:
                # pylint: disable=no-member
                logging.error(
                    f'Get password for GOA account {self[self.NAME]}, error '
                    + f'code {error.code}', exc_info=True)
                self.status.set(self.status.CREDENTIALS_MISSING)
            except Exception:
                logging.error(
                    'Unknown exception getting GOA credentials', exc_info=True)
                self.status.set(self.status.FAILED)
            return
        try:
            oauth2_based = self.goa_account.get_oauth2_based()
            auth_token = oauth2_based.call_get_access_token_sync(None)
            user_name = self.goa_account.get_mail().props.imap_user_name
        except GLib.Error as error:
            # pylint: disable=no-member
            if error.code == 0:
                self.status.set(self.status.OFFLINE)
            elif error.code == 4:
                logging.error('Locked or Invalid credentials for GOA account '
                              + self[self.NAME])
                self.status.set(self.status.CREDENTIALS_MISSING)
            else:
                logging.error('Unknown GLIB exception getting GOA credentials',
                              exc_info=True)
                self.status.set(self.status.FAILED)
        except Exception as error:
            logging.error(
                'Unknown exception getting GOA credentials', exc_info=True)
            self.status.set(self.status.FAILED)
        else:
            self[self.OAUTH2] = (
                f'user={user_name}\1auth=Bearer {auth_token[0]}\1\1')
            if self.status.error:
                self.status.set(self.status.SUCCESS)

    @classmethod
    def is_local(cls, account):
        return False

    @property
    def is_locked(self):
        if not self.goa_account:
            return True
        self.get_credentials()
        if self.goa_account.get_password_based():
            return self.PASS not in self or not self[self.PASS]
        return self.OAUTH2 not in self or not self[self.OAUTH2]

    @classmethod
    def sync(cls, config, commit=True):
        # Clean up accounts deleted in GOA
        for account in cls.list(config):
            found = False
            for goa_account in cls.goa_accounts():
                goa_mail = goa_account.get_mail().props
                if account[cls.UUID] == cls.get_uuid(goa_mail):
                    found = True
                    break
            if not found:
                account.delete(commit=False, keep_credentials=False)
        # Add new accounts for new GOA accounts
        for goa_account in cls.goa_accounts():
            goa_mail = goa_account.get_mail().props
            uuid = cls.get_uuid(goa_mail)
            if cls.get(config, uuid):
                logging.debug('Goa account for %s already registered',
                              goa_mail.email_address)
                continue
            account_dict = {
                cls.BACKEND:cls.IMAP, cls.UUID:uuid, cls.TYPE:cls.GOA,
                cls.NAME:_('Gnome account {0}').format(goa_mail.email_address)}
            account = cls(config, account_dict)
            if account.status.error:
                continue
            logging.info('Creating %s (%s)',
                         account[cls.NAME], account[cls.PROVIDER])
            account.section = cls.SECTION_NAME + uuid
            account.save(commit=False)
        if commit:
            config.save()

    @staticmethod
    def is_available():
        return bool(Goa)

    @classmethod
    def cleanup(cls, config):
        logging.debug('Cleanup GOA accounts')
        # Remove imported Goa accounts
        for account in cls.list(config):
            logging.info('Deleting %s', account[cls.NAME])
            account.delete(commit=False)

    @classmethod
    def list(cls, config):
        return filter(lambda a: a[a.TYPE] == a.GOA, super().list(config))

    @staticmethod
    def get_uuid(mail_account):
        digest = hashlib.md5(mail_account.email_address.encode('utf-8'))
        return digest.hexdigest()[:8]

    @staticmethod
    def goa_accounts():
        client = Goa.Client.new_sync(None)
        accounts = client.get_accounts()
        for account in accounts:
            mail = account.get_mail()
            if not mail or account.get_account().props.mail_disabled:
                continue
            yield account


class AccountStatus(dict):
    OFFLINE = -1
    SUCCESS = 0
    FAILED = 1
    REFUSED = 2
    BAD_LOGIN = 3
    CREDENTIALS_MISSING = 4
    CONNECT_ERROR = 5
    TIMEOUT = 6
    INVALID_PATH = 7
    UNKNOWN = 8
    SAVED_KEYS = (Account.UUID, Account.ERR_COUNT, Account.ERR_LAST,
                  Account.ERR_CODE, Account.ERR_MSG)
    _ITEMS = ((SUCCESS, ''),
              (OFFLINE, _('Offline')),
              (FAILED, _('Connection failed')),
              (REFUSED, _('Connection refused')),
              (BAD_LOGIN, _('Authentification failed')),
              (CREDENTIALS_MISSING, _('No credentials found !')),
              (CONNECT_ERROR, _('Connection error')),
              (TIMEOUT, _('Connection timeout')),
              (UNKNOWN, _('Unknown error')),
              (INVALID_PATH, _('No valid local mail folder/file found')))

    def __init__(self, account, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.account = account
        self.filepath = os.path.join(
            CACHE_DIR, f'{self.account[Account.UUID]}.status')
        for status_code, error_message in self._ITEMS:
            self[status_code] = error_message
        self[Account.UUID] = self.account[Account.UUID]
        self[Account.ERR_COUNT] = 0
        self[Account.ERR_LAST] = 0
        self[Account.ERR_CODE] = self.SUCCESS
        self[Account.ERR_MSG] = ''
        self[Account.FETCH_COUNT] = 0
        self.load()

    def __eq__(self, other):
        return self.error_code == other

    @property
    def error_code(self):
        return self[Account.ERR_CODE]

    @property
    def error_count(self):
        return self[Account.ERR_COUNT]

    @property
    def error_last(self):
        return self[Account.ERR_LAST]

    @property
    def error_message(self):
        if self[Account.ERR_MSG]:
            return self[Account.ERR_MSG]
        return self.error_code if self.error else ''

    @property
    def offline(self):
        return self.error_code == self.OFFLINE

    @property
    def error(self):
        return self.error_code > 0

    @property
    def timeout(self):
        return self.error_code == self.TIMEOUT

    @property
    def have_changed(self):
        cache_content = pickle_read(self.filepath)
        if not cache_content:
            return True
        return dict(self) != cache_content

    @property
    def fetch_count(self):
        return self[Account.FETCH_COUNT]

    def load(self):
        cache_content = pickle_read(self.filepath)
        if not cache_content or not isinstance(cache_content, dict):
            return
        for saved_key in cache_content:
            self[saved_key] = cache_content[saved_key]

    @property
    def serialized(self):
        return dict(filter(lambda si: si[0] in self.SAVED_KEYS,
                           dict(self).items()))

    def save(self):
        pickle_write(self.serialized, self.filepath)

    def get(self):
        return self.error_code or self.SUCCESS

    def increase_fetch_count(self):
        if self[Account.FETCH_COUNT] > 9:
            self[Account.FETCH_COUNT] = 0
            return
        self[Account.FETCH_COUNT] += 1

    def set(self, error_code, error_message=None):
        """ Update account status and user's error message """
        if error_code not in self:
            logging.error(f'Error code {error_code} is unknown !!!')
            return
        if error_code <= 0:
            self[Account.ERR_COUNT] = 0
        else:
            self[Account.ERR_LAST] = datetime.now().strftime('%s')
            if ((self.error_code == error_code and self.error_count < 3)
                    or not self.error_count):
                self[Account.ERR_COUNT] += 1
        self[Account.ERR_CODE] = error_code
        self[Account.ERR_MSG] = error_message or self[error_code]

    def set_success(self):
        self.set(self.SUCCESS)

    def set_offline(self):
        if not Account.is_local(self.account):
            self.set(self.OFFLINE)
