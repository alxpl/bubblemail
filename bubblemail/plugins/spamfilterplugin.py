#!/usr/bin/env python3
# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
from bubblemail.plugin import Plugin
from bubblemail.mail import Mail
from bubblemail.i18n import _
from bubblemail.config import str_to_list

class SpamfilterPlugin(Plugin):
    MANIFEST = (
        _("Spam Filter"), _("Filters out unwanted mails."),
        "1.2.0", "Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr")
    DEFAULT_CONFIG = {'filter_list' : 'newsletter, viagra',
                      'filter_from' : [Mail.NAME, Mail.ADDRESS, Mail.SUBJECT],
                      'case_sensitive' : False}


    def widget(self, widget_id):
        return self.builder.get_object(widget_id)

    @property
    def filter_list(self):
        return self.config['filter_list'].strip('\n').strip(' ').split(',')

    @property
    def txt_buffer(self):
        return self.widget('txt_buffer')

    def load_settings(self):
        self.txt_buffer.set_text(self.config['filter_list'])
        self.widget('subject_chk').set_active(
            Mail.SUBJECT in self.config['filter_from'])
        self.widget('from_chk').set_active(
            Mail.NAME in self.config['filter_from'])
        self.widget('recipients_chk').set_active(
            Mail.RECIPIENTS in self.config['filter_from'])
        self.widget('case_chk').set_active(bool(self.config['case_sensitive']))

    def save_settings(self):
        start, end = self.txt_buffer.get_bounds()
        self.config['filter_list'] = self.txt_buffer.get_text(start, end, False)
        self.config['filter_from'] = []
        if self.widget('from_chk').get_active():
            self.config['filter_from'] = [Mail.NAME, Mail.ADDRESS]
        if self.widget('subject_chk').get_active():
            self.config['filter_from'].append(Mail.SUBJECT)
        if self.widget('recipients_chk').get_active():
            self.config['filter_from'].append(Mail.RECIPIENTS)
        self.config['case_sensitive'] = self.widget('case_chk').get_active()

    def on_update(self, mails):
        new_mails, unseen_mails = mails
        return (list(filter(self.mail_filter, new_mails)),
                list(filter(self.mail_filter, unseen_mails)))

    def mail_filter(self, mail):
        is_spam = False
        filter_input = ' '.join(map(lambda k : mail[k],
                                    str_to_list(self.config['filter_from'])))
        if not self.config['case_sensitive']:
            filter_input = filter_input.lower()
        for filter_exp in self.filter_list:
            filter_exp = filter_exp.strip()
            if not filter_exp:
                continue
            if not self.config['case_sensitive']:
                filter_exp = filter_exp.lower()
            if filter_exp in filter_input:
                is_spam = True
                break
        return not is_spam
