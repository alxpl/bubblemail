# Copyright 2011 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2011 Ralf Hersel <ralf.hersel@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
import logging
import gi
# pylint: disable=C0413, W1202
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gdk, Gtk, GLib
import dbus
from dbus.mainloop.glib import DBusGMainLoop

from ..config import (PACKAGE_NAME, CACHE_DIR, APP_VERSION, APP_URL, APP_DESC,
                      DBUS_BUS_NAME, DBUS_OBJ_PATH, Config as C)
from ..i18n import _
from ..utils import (init_logging, set_log_level, get_data_file,
                     service_is_running)
from ..account import Account as A, RemoteAccount, LocalAccount
from ..plugin import PluginAgregator
from ..dbusservice import (CFG_UPDATE_SIG, CFG_SET_METHOD, REFRESH_METHOD,
                           PLGSTATE_GET_METHOD, STATUS_UPDATE_SIG)
from . import check_ready
from .rowwidget import AccountRow, PluginRow
from .accountdialog import AccountDialog

DBusGMainLoop(set_as_default=True)


# pylint: disable=R0904, R0914
class SettingsWindow:
    UNDO_DELAY = 8

    def __init__(self):
        self.ready = False
        self.config = C()
        init_logging(loglevel=self.config.log_level)
        self.config_lock = False
        self.window_size = None
        self.new_account = None
        self.on_account_edit = False
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('settings.ui'))
        self.widget = builder.get_object
        builder.connect_signals({
            'on_add': self.on_add,
            'on_back': self.on_back,
            'on_refresh': self.on_refresh,
            'on_logshow': self.on_logshow,
            'on_about_show': self.on_about_show,
            'on_about_closed': self.on_about_closed,
            'on_about_toggled': self.on_about_toggled,
            'on_credits_toggled': self.on_credits_toggled,
            'on_poll_changed': self.on_poll_changed,
            'on_loglevel_changed': self.on_loglevel_changed,
            'on_lbox_pages_row_activated': self.on_lbox_pages_row_activated,
            'on_window_deleted': self.on_window_deleted,
            'on_auth_warn_dlg_closed': self.on_auth_warn_dlg_closed,
            'on_service_error_dlg_close': self.on_window_deleted,
            'on_window_resize': self.on_window_resize
        })
        if not service_is_running():
            self.widget('service_error_dlg').show()
        else:
            self.ui_init()
        Gtk.main()

    def ui_init(self):
        dbus_loop = dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SessionBus(dbus_loop)
        self.dbus_proxy = None
        self.dbus_proxy = bus.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
        self.dbus_proxy.connect_to_signal(CFG_UPDATE_SIG, self.on_config_change)
        self.dbus_proxy.connect_to_signal(
            STATUS_UPDATE_SIG, self.on_status_change)
        self.widget_id = lambda w: w.get_name().split('_')[-1]
        self.widget('main_stk').set_visible_child_name('accounts')
        self.window = self.widget('settings_wdw')
        self.window.set_title(_('Bubblemail settings'))
        self.window.set_icon_name(PACKAGE_NAME)
        provider = Gtk.CssProvider()
        provider.load_from_path(get_data_file('settings.css'))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.window_size = self.config.get_list(C.CORE, C.WINDOW_SIZE)
        self.window_size = tuple(map(int, self.window_size))
        if 0 not in self.window_size:
            self.widget('settings_wdw').set_default_size(self.window_size[0],
                                                         self.window_size[1])
        self.ui_update()
        self.window.show()
        # if self.config.get_core(C.UPDATE_AVAILABLE) == '1':
        #     if self.widget('new_version_dlg').run() == Gtk.ResponseType.OK:
        #         webbrowser.open(DOWNLOAD_URL)
        #     self.widget('new_version_dlg').destroy()

    def ui_update(self):
        self.ready = False
        # Logging level
        self.widget('loglevel_swt').set_active(
            int(self.config.get_core(C.DEBUG)))
        accounts = list(A.list(self.config))
        on_main = self.widget('global_stk').get_visible_child_name() == 'main'
        focus = self.window.get_focus()
        if not accounts and on_main and not focus and not self.on_account_edit:
            logging.debug('No accounts')
            self.widget('global_stk').set_visible_child_name('newaccount')
            self.widget('leftbtn_stk').set_visible_child_name('empty')
            self.widget('header_stk').set_visible_child_name('appname')
            self.widget('main_box').hide()
            self.widget('settings_wdw').resize(1, 1)
            self.ready = True
            return
        self.widget('accounts_lbox').set_visible(bool(accounts))
        if self.widget('global_stk').get_visible_child_name() == 'newaccount':
            self.widget('main_box').show()
            for stack_widget in 'global_stk', 'leftbtn_stk', 'header_stk':
                self.widget(stack_widget).set_visible_child_name('main')
        # Poll interval
        for btn in self.widget('pollmenu_box'):
            if btn.get_name() == self.config.get_core(C.POLL_INTERVAL):
                self.widget('pollbtn_lbl').set_text(btn.get_property('text'))
                break
        # Accounts
        for row in self.widget('accounts_lbox'):
            row.destroy()
        for account in accounts:
            AccountRow(self, account)
        # Plugins
        plugins_lbox = self.widget('plugins_lbox')
        for row in plugins_lbox:
            row.destroy()
        plugin_error = self.dbus_send(PLGSTATE_GET_METHOD, response=True)
        plugin_agregator = PluginAgregator(config=self.config)
        for plug_name in plugin_agregator.list():
            plugin = plugin_agregator[plug_name]
            PluginRow(self, plugin_error or [], plug_name, plugin)
        self.ready = True

    @check_ready
    def on_window_resize(self, window, unused_value):
        window_size = window.get_size()
        if 0 in window_size or self.window_size == window_size:
            return
        self.window_size = window_size

    def on_lbox_pages_row_activated(self, unused_tree, row):
        ''' Split between Accounts and plugins page '''
        page_request = row.get_name()
        main_stk = self.widget('main_stk')
        if page_request == main_stk.get_visible_child_name():
            return
        main_stk.set_visible_child_name(page_request)
        self.widget('add_btn').set_sensitive(page_request == 'accounts')

    def on_config_change(self, serialized_config):
        if self.config_lock:
            return
        self.config = C(load=False)
        self.config.load_serialized(serialized_config)
        self.ui_update()

    def on_status_change(self, unused_serialized_status):
        self.ui_update()

    @check_ready
    def on_loglevel_changed(self, unused_widget, state):
        self.config.set_core(C.DEBUG, int(state))
        set_log_level(self.config)
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())

    def on_about_toggled(self, widget):
        self.widget('app_icon').set_pixel_size(-1)
        self.widget('credits_btn').set_active(not widget.get_active())
        page = 'about' if widget.get_active() else 'credits'
        self.widget('stk_credits').set_visible_child_name(page)

    def on_credits_toggled(self, widget):
        self.widget('app_icon').set_pixel_size(128)
        self.widget('about_btn').set_active(not widget.get_active())
        page = 'credits' if widget.get_active() else 'about'
        self.widget('stk_credits').set_visible_child_name(page)

    def on_refresh(self, unused_widget):
        self.dbus_send(REFRESH_METHOD)

    def set_log_scroll(self):
        vadj = self.widget('log_scroll').get_vadjustment()
        vadj.set_value(vadj.get_upper())

    @check_ready
    def on_logshow(self, unused_widget):
        for stack_name in 'global_stk', 'header_stk', 'leftbtn_stk':
            self.widget(stack_name).set_visible_child_name('log')
        self.widget('main_btn').hide()
        log_file = os.path.join(CACHE_DIR, f'{PACKAGE_NAME}.log')
        if not os.path.isfile(log_file):
            self.widget('logbuffer').set_text(_('No logfile found'))
            return
        log_content = ''
        try:
            with open(log_file, 'r', encoding='utf-8') as log:
                log_content = log.read()
        except (PermissionError, OSError) as exc_error:
            log_content = exc_error.strerror
        self.widget('logbuffer').set_text(log_content)
        GLib.idle_add(self.set_log_scroll)

    @check_ready
    def on_back(self, unused_widget):
        for stack_name in 'global_stk', 'header_stk', 'leftbtn_stk':
            self.widget(stack_name).set_visible_child_name('main')
        self.widget('main_btn').show()

    @check_ready
    def on_add(self, widget):
        if self.widget('main_stk').get_visible_child_name() != 'accounts':
            return
        backend = widget.get_name()
        is_local = backend in LocalAccount.BACKENDS
        is_imap = backend == RemoteAccount.IMAP
        account_class = LocalAccount if is_local else RemoteAccount
        self.new_account = account_class(self.config, {A.BACKEND: backend})
        if is_imap and self.config.getboolean(C.CORE, C.SHOW_AUTH_WARN):
            self.widget('auth_warn_dlg').show()
            return
        AccountDialog(self, self.new_account, create=True)

    @check_ready
    def on_poll_changed(self, widget):
        self.widget('pollbtn_lbl').set_text(widget.get_property('text'))
        self.config.set_core(C.POLL_INTERVAL, widget.get_name())
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())

    def on_auth_warn_dlg_closed(self, dialog, _unused_data=None):
        show_auth_warn = not self.widget('auth_warn_chk').get_active()
        dialog.hide()
        if show_auth_warn != bool(int(self.config.get_core(C.SHOW_AUTH_WARN))):
            self.config.set_core(C.SHOW_AUTH_WARN, show_auth_warn)
            self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        AccountDialog(self, self.new_account, create=True)

    @check_ready
    def on_window_deleted(self, unused_widget, unused_event=None):  # pylint: disable=R0201
        self.ready = False
        old_window_size = self.config.get_list(C.CORE, C.WINDOW_SIZE)
        if self.window_size != old_window_size:
            logging.debug('Saving new window size')
            self.config.set_core(C.WINDOW_SIZE, self.window_size)
            self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        GLib.idle_add(Gtk.main_quit)

    def dbus_send(self, *signals, arg=None, response=False):
        pxy = self.dbus_proxy

        def _send(signal):
            try:
                logging.debug(f'Sending dbus signal "{signal}"')
                if arg is None:
                    resp = pxy.get_dbus_method(signal, DBUS_BUS_NAME)()
                else:
                    resp = pxy.get_dbus_method(signal, DBUS_BUS_NAME)(arg)
                return resp[0] if resp else None
            except Exception:
                logging.error(f'Dbus signal "{signal}"', exc_info=True)
                return None

        if response:
            return _send(signals[0])
        for signal in signals:
            GLib.idle_add(_send, signal)

    @check_ready
    def on_about_closed(self, unused_widget, unused_event=None):
        self.widget('about_dlg').hide()
        return True

    @check_ready
    def on_about_show(self, unused_widget):
        if str(APP_VERSION) in self.widget('app_version').get_text():
            self.widget('about_dlg').show()
            return
        self.widget('app_name').set_text(PACKAGE_NAME.capitalize())
        self.widget('app_version').set_text(str(APP_VERSION))
        self.widget('app_desc').set_text(_(APP_DESC))
        self.widget('app_website').set_markup(
            '<a href="http://bubblemail.free.fr">Website</a>  '
            + f'<a href="{APP_URL}">Repository</a>')
        credits_buf = self.widget('credits_buf')
        add_name = lambda n: credits_buf.insert_at_cursor(f'            {n}\n')
        add_vertical_space = lambda: credits_buf.insert_markup(
            credits_buf.get_end_iter(), '<span size="xx-small">\n</span>', -1)
        credits_buf.set_text(_('Created by') + '\n')
        add_name('Razer')
        add_name('Alexander Ploumistos')
        add_name('Christoph W. Roeper')
        add_name('Robert-André Mauchin')
        add_name('Patrick Ulbrich')
        add_name('Ralf Hersel')
        add_name('Timo Kankare')
        add_name('Leighton Earl')
        add_name('Marco Ferragina')
        add_vertical_space()
        credits_buf.insert_at_cursor(_('Icons & Design') + '\n')
        add_name('Alexey Varfolomeev')
        add_vertical_space()
        credits_buf.insert_at_cursor(_('Documented by') + '\n')
        add_name('Razer')
        add_name('Alexander Ploumistos')
        add_vertical_space()
        credits_buf.insert_at_cursor(_('Translated by') + '\n')
        add_name('Razer')
        add_name('Alexander Ploumistos')
        add_name('Alyssa Cohen')
        add_vertical_space()
        credits_buf.insert_at_cursor(_('Packaged by') + '\n')
        add_name('Alexander Ploumistos')
        add_name('Robert-André Mauchin')
        add_name('Erich Eickmeyers')
        add_name('Thomas Ward')
        self.widget('about_dlg').show()
