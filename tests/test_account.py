# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

"""Test cases for Account."""
# TODO: GOA tests
import os
import sys
import shutil
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413
from bubblemail.config import Config, FAKESPACE
from bubblemail.account import (Account as A, AccountStatus as AS,
                                RemoteAccount as RA, LocalAccount as LA)

# pylint: disable=W0621
@pytest.fixture
def fake_config_folder():
    if os.path.isdir('/tmp/bubblemail-test'):
        shutil.rmtree('/tmp/bubblemail-test')
    return '/tmp/bubblemail-test'

class FakeSecretStore:
    @staticmethod
    def save(unused_account_name, unused_uuid, unused_password):
        return True

    @staticmethod
    def password(unused_uuid):
        return 'secret'

    def clear(self, uuid):
        pass

@pytest.fixture
def fake_secret_store():
    return FakeSecretStore

def test_default_values(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config)
    assert len(account[A.UUID]) >= 8 and account.section and account[A.ENABLED]
    assert account[A.TYPE] == A.INTERNAL
    local_account = LA(config)
    assert local_account[A.BACKEND] == A.MAILDIR
    assert local_account[A.FOLDERS] == []
    remote_account = RA(config)
    assert remote_account[A.BACKEND] == A.IMAP
    assert remote_account[A.FOLDERS] == []
    for boolean_key in A.ENABLED, RA.UNSECURE:
        assert isinstance(remote_account[boolean_key], bool)
    assert remote_account[A.ENABLED]
    assert not remote_account[RA.UNSECURE]

def test_null_folder_filter(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=['a', '', 'def'])
    assert account[A.FOLDERS] == ['a', 'def']

def test_account_folder_list_from_string(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=f'a,r, f{FAKESPACE}with{FAKESPACE}s, c, def')
    assert account[A.FOLDERS] == ['a,r', 'f with s', 'c', 'def']

def test_account_config_section_name(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, uuid='12345678')
    assert account.section == 'Account 12345678'

def test_account_uuid_is_unique(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account_map = [A(config, name='a', user='x', server='xx'),
                   A(config, name='b', backend=A.POP3, user='x', server='xx'),
                   A(config, name='c', user='x', server='xx')]
    assert len(set(a[A.UUID] for a in account_map)) == len(account_map)

def test_account_validation(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    local_account = LA(config)
    assert not local_account.is_valid
    local_account[A.NAME] = 'valid account'
    local_account[LA.PATH] = '/tmp/Maildir'
    assert not local_account.is_valid
    os.makedirs(local_account[LA.PATH])
    assert local_account.is_valid
    os.rmdir(local_account[LA.PATH])
    with open(local_account[LA.PATH], 'a', encoding='utf-8') as la_file:
        la_file.close()
    assert not local_account.is_valid
    local_account[LA.BACKEND] = A.MBOX
    assert local_account.is_valid
    for key in A.NAME, LA.PATH:
        valid_value = local_account[key]
        local_account[key] = ''
        assert not local_account.is_valid
        local_account[key] = valid_value
    assert local_account.is_valid
    os.remove(local_account[LA.PATH])
    local_account[A.BACKEND] = 'unsupportedBackend'
    assert not local_account.is_valid
    remote_account = RA(config)
    assert not remote_account.is_valid
    remote_account[A.NAME] = 'valid account'
    remote_account[RA.SERVER] = 'imap.valid.server.org'
    remote_account[RA.USER] = 'valid username'
    remote_account[RA.PASS] = 'validpassword'
    assert remote_account.is_valid
    for key in A.NAME, RA.SERVER, RA.USER, RA.PASS:
        valid_value = remote_account[key]
        remote_account[key] = ''
        assert not remote_account.is_valid
        remote_account[key] = valid_value
    assert remote_account.is_valid
    remote_account[RA.PROXY] = True
    assert not remote_account.is_valid
    remote_account[RA.PROXY_HOST] = 'localhost'
    remote_account[RA.PROXY_PORT] = '9050'
    assert remote_account.is_valid
    remote_account[A.BACKEND] = 'unsupportedBackend'
    assert not remote_account.is_valid

def test_account_keep_configuration_across_save(mocker, fake_config_folder,
                                                fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_template = dict(
        name='my name', folders=['a', 'b'], type=A.INTERNAL, backend=A.MAILDIR,
        path='/home/user/Maildir', enabled=False)
    local_account = LA(config)
    for key, value in account_template.items():
        local_account[key] = value
    local_account.save()
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    for key, value in account_template.items():
        assert saved_account[key] == value

    account_template = dict(
        name='my name', user='who', password='secret', server='example.org',
        port='124', folders=['a', 'b'], type=A.INTERNAL, backend=A.IMAP,
        enabled=False, unsecure=True, webmail='http://mywebmail.com',
        proxy=True, pxyurl='localhost', pxyport='9050', pxyuser='pxyuser',
        pxytype='4', pxypass='pxypass')
    remote_account = RA(config)
    for key, value in account_template.items():
        remote_account[key] = value
    remote_account.save()
    config.load()
    saved_account = RA.get(config, remote_account[A.UUID])
    for key, value in account_template.items():
        print(key)
        print(value)
        print(saved_account[key])
        assert saved_account[key] == value

def test_account_list(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_map = [LA(config, name='a', bavkend=A.MBOX, path='/home/user/.mbx'),
                   RA(config, name='b', backend=A.POP3, user='x', server='xx'),
                   RA(config, name='c', user='x', server='xx')]
    for account in account_map:
        account.save()
    assert list(A.list(config)) == account_map

def test_account_delete(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    remote_account = RA(config)
    account_uuid = remote_account[A.UUID]
    remote_account.save()
    remote_account = A.get(config, account_uuid)
    remote_account.delete()
    assert not A.get(config, account_uuid)

def test_account_set_status(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_template = dict(
        name='my name', folders=['a', 'b'], type=A.INTERNAL, backend=A.MAILDIR,
        path='/home/user/Maildir', enabled=False)
    local_account = LA(config)
    for key, value in account_template.items():
        local_account[key] = value
    # pylint: disable=no-member
    local_account.status.set(AS.INVALID_PATH)
    # Account should not be created on set_status call
    assert not LA.get(config, local_account[A.UUID])
    local_account = LA(config)
    status = local_account.status
    for key, value in account_template.items():
        local_account[key] = value
    local_account.save()
    status.set(status.INVALID_PATH)
    status.save()
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    status = saved_account.status
    assert status.error_code == AS.INVALID_PATH
    assert status.error_message == status[AS.INVALID_PATH]
    status.set_success()
    status.save()
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    status = saved_account.status
    assert status.error_code == AS.SUCCESS
    assert not status.error_message
    assert not status.error_count
    status.set(status.TIMEOUT)
    assert status.error
    assert status.error_code == status.TIMEOUT
    assert status.error_message == status[status.TIMEOUT]
    assert status.error_count == 1
    status.set(status.TIMEOUT)
    assert status.error_count == 2
    status.set(status.TIMEOUT)
    assert status.error_count == 3
    status.set(status.TIMEOUT)
    assert status.error_count == 3
    status.set(status.OFFLINE)
    assert status.error_count == 0
    # pylint: enable=no-member

def test_account_fetch_count(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    remote_account = RA(config)
    assert not remote_account.status.fetch_count
    remote_account.status.increase_fetch_count()
    assert remote_account.status.fetch_count == 1
    for _ in range(10):
        remote_account.status.increase_fetch_count()
    assert remote_account.status.fetch_count == 0
