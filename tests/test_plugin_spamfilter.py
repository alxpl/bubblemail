# Copyleft 2019/2021 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
""" Test cases for the spamfilter plugin """
import sys
import email
sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, W0621, W0212, W0231, W0233, R0903, R0914, import-error
from bubblemail.mail import Mail as M
from bubblemail.plugins.spamfilterplugin import SpamfilterPlugin

MAIL_STRING = """\
from: Ugly Helloworld <hello@world.com>
To: Me <me@example.org>, Other <me@example.org>
Cc: Third guy <tg@example.org>, thisone <this@one.com>
subject: I am spam...
Date: Tue, 01 May 2018 16:28:08 +0300

...World!
"""

MAIL_TEMPLATE = email.message_from_string(MAIL_STRING)

def test_spamfilter():
    sf_plugin = SpamfilterPlugin()
    sf_plugin.config['filter_from'] = f'{M.NAME}, {M.ADDRESS}'
    sf_plugin.config['filter_list'] = 'unfound'
    sf_plugin.config['case_sensitive'] = False
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE)
    result, _ = sf_plugin.on_update([[mail], []])
    assert result
    sf_plugin.config['filter_list'] = 'World'
    result, _ = sf_plugin.on_update([[mail], []])
    assert not result
    sf_plugin.config['case_sensitive'] = True
    result, _ = sf_plugin.on_update([[mail], []])
    assert result
    sf_plugin.config['case_sensitive'] = False
    sf_plugin.config['filter_from'] = f'{M.NAME}, '
    sf_plugin.config['filter_list'] = 'idontknpw, transit, ugly'
    result, _ = sf_plugin.on_update([[mail], []])
    assert not result
    sf_plugin.config['filter_from'] = f'{M.NAME}, {M.RECIPIENTS}'
    sf_plugin.config['filter_list'] = 'unfound, example'
    result, _ = sf_plugin.on_update([[mail], []])
    assert not result
    sf_plugin.config['filter_from'] = f'{M.RECIPIENTS}'
    sf_plugin.config['filter_list'] = 'thisone'
    result, _ = sf_plugin.on_update([[mail], []])
    assert not result
    sf_plugin.config['filter_from'] = f'{M.RECIPIENTS}, {M.SUBJECT}'
    sf_plugin.config['filter_list'] = 'am spam'
    result, _ = sf_plugin.on_update([[mail], []])
    assert not result
