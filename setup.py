#!/usr/bin/env python3

# Copyright 2019, 2022 razer <razerraz@free.fr>
# Copyright 2011 - 2019 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston ,
# MA 02110-1301, USA.

import os
import glob
from setuptools import setup, find_packages
from setuptools.command.build_py import build_py

from bubblemail.config import PACKAGE_NAME, APP_VERSION, APP_URL, APP_DESC
from build_tools import (BUILD_BIN_DIR, BUILD_LOCALE_DIR, build_avatar_provider,
                         create_default_avatars, gen_locales)


class BuildData(build_py):
    def run(self):
        os.makedirs(BUILD_BIN_DIR, exist_ok=True)
        # generate translations
        gen_locales()
        build_py.run(self)
        build_avatar_provider()
        create_default_avatars()
        avatar_provider_bin = os.path.join(BUILD_BIN_DIR,
                                           f'{PACKAGE_NAME}-avatar-provider')
        if os.path.isfile(avatar_provider_bin):
            return
        with open(avatar_provider_bin, 'w', encoding='utf-8') as fake_provider:
            fake_provider.write('#!/bin/bash\n')
        os.chmod(avatar_provider_bin, 0o755)

def get_files(src_path, ext):
    file_map = glob.glob(f'{src_path}/**/{PACKAGE_NAME}.{ext}', recursive=True)
    root = src_path.split('/')[0]
    path_map = map(lambda f:f.replace(root, 'share'), file_map)
    path_map = map(lambda f:f.replace(f'{PACKAGE_NAME}.{ext}', ''), path_map)
    return list(zip(path_map, map(lambda f: [f], file_map)))

setup(
    name=PACKAGE_NAME,
    version=APP_VERSION,
    description=APP_DESC,
    url=APP_URL,
    long_description=(
        'Bubblemail is a DBus service providing a list of the new and unread '
        + 'user\'s mail from local mailboxes, pop, imap, and gnome online '
        + 'accounts. It includes a libnotify frontend to create notifications '
        + 'and can be used by other frontends as well.'),
    author='Razer',
    author_email='razerraz@free.fr',
    license='LGPL-2.0-or-later',
    include_package_data=True,
    classifiers=[
        'Topic :: Communications :: Email',
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: GTK',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',  #pylint: disable=line-too-long
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='mail gnome account notification dbus gtk',
    packages=find_packages(),
    scripts=[f'bin/{PACKAGE_NAME}d', f'bin/{PACKAGE_NAME}'],
    data_files=[
        (f'share/{PACKAGE_NAME}/plugins', glob.glob('data/plugins/*.ui')),
        ('bin',
         [os.path.join(BUILD_BIN_DIR, f'{PACKAGE_NAME}-avatar-provider')]),
        (f'share/{PACKAGE_NAME}',
         glob.glob('data/*.ui') + glob.glob('data/*.css')),
        (f'share/{PACKAGE_NAME}/avatars', glob.glob('data/avatars/*.png')),
        (f'share/{PACKAGE_NAME}',
         [f'data/{PACKAGE_NAME}.ogg'] + glob.glob('data/*.png')),
        ('share/metainfo', [f'data/{PACKAGE_NAME}.appdata.xml']),
        ('share/applications', [f'data/{PACKAGE_NAME}.desktop']),
        ('/etc/xdg/autostart', [f'data/{PACKAGE_NAME}d.desktop']),
        ('share/man/man1', [f'data/{PACKAGE_NAME}.1']),
        ('share/man/man1', [f'data/{PACKAGE_NAME}d.1']),
        ] + get_files('data/icons', 'svg') + get_files(BUILD_LOCALE_DIR, 'mo'),
    cmdclass={'build_py': BuildData}
)
